//
//  DoctorLoginViewController.m
//  Mobile_HealtCare_System
//
//  Created by Umut Bozkurt on 12/20/14.
//  Copyright (c) 2014 Can Kerimoglu. All rights reserved.
//

#import "DoctorLoginViewController.h"

@interface DoctorLoginViewController ()

@property (nonatomic, strong) PFUser *user;

@end

@implementation DoctorLoginViewController

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    self.user = [PFUser user];
    self.tcnoTextField.delegate = self;
}

-(IBAction)login:(id)sender
{
    NSString *username = [self.tcnoTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *password = [self.passwordTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if (username.length < 11)
    {
        [[[UIAlertView alloc] initWithTitle:@"Warning!" message:@"TC No must be exactly 11 characters." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        return;
    }
    if (password.length < 2)
    {
        [[[UIAlertView alloc] initWithTitle:@"Warning!" message:@"Password must be at least 2 characters." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        return;
    }
    
    self.user.username = username;
    self.user.password = password;
    
    PFQuery *query = [PFUser query];
    [query whereKey:@"username" equalTo:self.tcnoTextField.text];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error)
        {
            if (objects.count > 0)
            {
                self.user = [PFUser logInWithUsername:self.tcnoTextField.text password:self.passwordTextField.text];
                
                if (self.user)
                {
                    PFQuery *query = [Doctor query];
                    [query whereKey:@"user" equalTo:[PFUser currentUser]];
                    
                    NSArray *results = [query findObjects];
                    Doctor *doctor;
                    
                    if (results.count > 0)
                        doctor = [results firstObject];
                    else
                    {
                        doctor = [Doctor object];
                        doctor.fullName = self.fullNameTextField.text;
                        doctor.user = [PFUser currentUser];
                        [doctor save];
                    }
                    
                    [self performSegueWithIdentifier:@"doctorLogin" sender:doctor];
                }
                else
                {
                    [[[UIAlertView alloc] initWithTitle:@"Wrong Credentials" message:@"TC No or Password is incorrect." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
                    self.loginButton.enabled = YES;
                }
            }
            else
            {
                [self.user signUp];
                
                PFQuery *query = [Doctor query];
                [query whereKey:@"user" equalTo:[PFUser currentUser]];
                
                NSArray *results = [query findObjects];
                Doctor *doctor;
                
                if (results.count > 0)
                    doctor = [results firstObject];
                else
                {
                    doctor = [Doctor object];
                    doctor.fullName = self.fullNameTextField.text;
                    doctor.user = [PFUser currentUser];
                    [doctor save];
                }
                
                [self performSegueWithIdentifier:@"doctorLogin" sender:doctor];
            }
        }
        else
        {
            [[[UIAlertView alloc] initWithTitle:@"Error" message:@"Somethings wrong, please try again later" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        }
    }];
    
    self.loginButton.enabled = NO;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([textField isEqual:self.tcnoTextField])
    {
        NSUInteger newLength = textField.text.length + string.length - range.length;
        return newLength>11?NO:YES;
    }
    
    return YES;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    self.loginButton.enabled = YES;
    
    CheckAppointmentsViewController *vc = segue.destinationViewController;
    vc.doctor = sender;
}

@end
