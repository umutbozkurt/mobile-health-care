//
//  FirstViewController.h
//  Mobile_HealtCare_System
//
//  Created by CanCan on 13/12/14.
//  Copyright (c) 2014 Can Kerimoglu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Appointment.h"
#import "Doctor.h"
#import "Patient.h"
#import "ListAppointmentsTableViewController.h"

@interface GetAppointmentViewController : UIViewController<UIAlertViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate>
@property (weak, nonatomic) IBOutlet UITextField *patientTcNo;
@property (weak, nonatomic) IBOutlet UITextField *patientName;
@property (weak, nonatomic) IBOutlet UITextField *patientDisease;
@property (weak, nonatomic) IBOutlet UIPickerView *doctorPickerView;

@property (nonatomic, strong) Patient *patient;

- (IBAction)btnAppointment:(id)sender;

@end

