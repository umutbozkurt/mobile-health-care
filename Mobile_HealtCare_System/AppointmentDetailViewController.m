//
//  AppointmentDetailViewController.m
//  Mobile_HealtCare_System
//
//  Created by Umut Bozkurt on 12/20/14.
//  Copyright (c) 2014 Can Kerimoglu. All rights reserved.
//

#import "AppointmentDetailViewController.h"

@interface AppointmentDetailViewController ()
@property (weak, nonatomic) IBOutlet UIButton *saveButton;

@end

@implementation AppointmentDetailViewController

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    Patient *patient = (Patient *)[self.appointment.patient fetchIfNeeded];
    
    self.fullNameLabel.text = [self.fullNameLabel.text stringByAppendingString:patient.fullName];
    self.diseaseLabel.text = [self.diseaseLabel.text stringByAppendingString:self.appointment.disease];
    
    if (self.patientMode)
    {
        UILabel *diagLabel = [[UILabel alloc] initWithFrame:self.diagnosisTextfield.frame];
        diagLabel.textAlignment = NSTextAlignmentCenter;
        diagLabel.text = [NSString stringWithFormat:@"Diagnosis: %@", self.appointment.diagnosis];
        
        UILabel *mediLabel = [[UILabel alloc] initWithFrame:self.medicineTextfield.frame];
        mediLabel.textAlignment = NSTextAlignmentCenter;
        mediLabel.text = [NSString stringWithFormat:@"Medicine: %@", self.appointment.medicines];
        
        [self.view addSubview:diagLabel];
        [self.view addSubview:mediLabel];
        
        self.medicineTextfield.placeholder = @"";
        self.diagnosisTextfield.placeholder = @"";
        
        self.diagnosisTextfield.enabled = NO;
        self.medicineTextfield.enabled = NO;
        self.saveButton.enabled = NO;
    }
}

-(IBAction)save:(id)sender
{
    Appointment *appointment = self.appointment;
    
    NSString *diagnosis = [self.diagnosisTextfield.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *medicines = [self.medicineTextfield.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if (!(diagnosis.length == 0 || medicines.length == 0))
    {
        appointment.diagnosis = diagnosis;
        appointment.medicines = medicines;
        appointment.done = YES;
        
        if([appointment save])
        {
            [[[UIAlertView alloc] initWithTitle:@"Success" message:@"Diagnosis and medicines are saved successfully" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        }
        else
        {
            [[[UIAlertView alloc] initWithTitle:@"Error!" message:@"Error while saving appointment details. Try again later." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        }

    }
    else
    {
        [[[UIAlertView alloc] initWithTitle:@"Warning!" message:@"Diagnosis or medicine cannot be empty!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    }
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
