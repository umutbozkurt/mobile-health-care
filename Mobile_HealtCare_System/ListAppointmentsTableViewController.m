//
//  ListAppointmentsTableViewController.m
//  Mobile_HealtCare_System
//
//  Created by Umut Bozkurt on 12/20/14.
//  Copyright (c) 2014 Can Kerimoglu. All rights reserved.
//

#import "ListAppointmentsTableViewController.h"

@interface ListAppointmentsTableViewController ()

@property (nonatomic, strong) NSArray *activeAppointments;
@property (nonatomic, strong) NSArray *doneAppointments;

@end

@implementation ListAppointmentsTableViewController

-(void)viewDidLoad
{
    [super viewDidLoad];
    self.activeAppointments = [NSArray array];
    self.doneAppointments = [NSArray array];
    
    [self fetchData];
}

-(void)fetchData
{
    PFQuery *query = [Appointment query];
    [query whereKey:@"patient" equalTo:self.patient];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        NSPredicate *donePred = [NSPredicate predicateWithFormat:@"done == YES"];
        NSPredicate *activePred = [NSPredicate predicateWithFormat:@"done == NO"];
        
        self.doneAppointments = [objects filteredArrayUsingPredicate:donePred];
        self.activeAppointments = [objects filteredArrayUsingPredicate:activePred];
        
        [self.tableView reloadData];
    }];
}

#pragma mark - Table view data source

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0)
    {
        if (self.activeAppointments.count == 0)
        {
            return 1;
        }
        else
        {
            return self.activeAppointments.count;
        }
    }
    else
    {
        if (self.doneAppointments.count == 0)
        {
            return 1;
        }
        else
        {
            return self.doneAppointments.count;
        }
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    Appointment *appointment;
    
    if (indexPath.section == 0)
    {
        if(self.activeAppointments.count == 0)
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"emptyCell" forIndexPath:indexPath];
            cell.textLabel.text = @"You have no pending appointments.";
            cell.userInteractionEnabled = NO;
            cell.accessoryType = UITableViewCellAccessoryNone;
            return cell;
        }
        else
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"appCell" forIndexPath:indexPath];
            cell.userInteractionEnabled = YES;
            appointment = self.activeAppointments[indexPath.row];
        }
    }
    else
    {
        if(self.doneAppointments.count == 0)
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"emptyCell" forIndexPath:indexPath];
            cell.textLabel.text = @"You have no pending appointments.";
            cell.userInteractionEnabled = NO;
            cell.accessoryType = UITableViewCellAccessoryNone;
            return cell;
        }
        else
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"appCell" forIndexPath:indexPath];
            cell.userInteractionEnabled = YES;
            appointment = self.doneAppointments[indexPath.row];
        }
    }
    
    cell.textLabel.text = appointment.disease;
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@", appointment.createdAt];
    
    return cell;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 0)
        return @"Pending Appointments";
    else
        return @"Past Appointments";
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 72;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        [self performSegueWithIdentifier:@"detail" sender:self.activeAppointments[indexPath.row]];
    }
    else
    {
        [self performSegueWithIdentifier:@"detail" sender:self.doneAppointments[indexPath.row]];
    }
}

- (IBAction)refresh:(UIRefreshControl *)sender
{
    [self fetchData];
    
    [sender endRefreshing];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    AppointmentDetailViewController *vc = segue.destinationViewController;
    vc.appointment = sender;
    vc.patientMode = YES;
}

@end
