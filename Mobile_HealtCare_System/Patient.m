//
//  Patient.m
//  Mobile_HealtCare_System
//
//  Created by Umut Bozkurt on 12/13/14.
//  Copyright (c) 2014 Can Kerimoglu. All rights reserved.
//

#import "Patient.h"
#import <Parse/PFObject+Subclass.h>

@implementation Patient

@dynamic user, fullName;

+(void)load
{
    [self registerSubclass];
}

+(NSString *)parseClassName
{
    return @"Patient";
}

@end
