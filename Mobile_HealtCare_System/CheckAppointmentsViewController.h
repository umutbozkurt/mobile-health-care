//
//  SecondViewController.h
//  Mobile_HealtCare_System
//
//  Created by CanCan on 13/12/14.
//  Copyright (c) 2014 Can Kerimoglu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Doctor.h"
#import "Patient.h"
#import "Appointment.h"
#import "AppointmentDetailViewController.h"

@interface CheckAppointmentsViewController : UIViewController<UIAlertViewDelegate, UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) Doctor *doctor;

@end

