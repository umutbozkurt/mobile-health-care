//
//  Appointment.m
//  Mobile_HealtCare_System
//
//  Created by Umut Bozkurt on 12/13/14.
//  Copyright (c) 2014 Can Kerimoglu. All rights reserved.
//

#import "Appointment.h"
#import <Parse/PFObject+Subclass.h>

@implementation Appointment

@dynamic doctor, patient, diagnosis, medicines, disease, done;

+(void)load
{
    [self registerSubclass];
}

+(NSString *)parseClassName
{
    return @"Appointment";
}

@end
