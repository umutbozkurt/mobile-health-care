//
//  PatientLoginViewController.m
//  Mobile_HealtCare_System
//
//  Created by Umut Bozkurt on 12/20/14.
//  Copyright (c) 2014 Can Kerimoglu. All rights reserved.
//

#import "PatientLoginViewController.h"

@interface PatientLoginViewController ()

@property (nonatomic, strong) PFUser *user;

@end

@implementation PatientLoginViewController

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    self.user = [PFUser user];
    self.tcnoTextfield.delegate = self;
}

-(IBAction)login:(id)sender
{
    NSString *username = [self.tcnoTextfield.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *password = [self.passwordTextfield.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if (username.length < 11)
    {
        [[[UIAlertView alloc] initWithTitle:@"Warning!" message:@"TC No must be exactly 11 characters." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        
        return;
    }
    if (password.length < 2)
    {
        [[[UIAlertView alloc] initWithTitle:@"Warning!" message:@"Password must be at least 2 characters." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        
        return;
    }
    
    self.user.username = username;
    self.user.password = password;
    
    self.user = [PFUser logInWithUsername:self.tcnoTextfield.text password:self.passwordTextfield.text];
    
    if (self.user)
    {
        PFQuery *query = [Patient query];
        [query whereKey:@"user" equalTo:self.user];
        
        NSArray *results = [query findObjects];
        Patient *patient;
        
        if (results.count > 0)
            patient = [results firstObject];
        else
        {
            patient = [Patient object];
            patient.fullName = self.fullnameTextfield.text;
            patient.user = [PFUser currentUser];
            [patient save];
        }
        
        [self performSegueWithIdentifier:@"patientLogin" sender:patient];
    }
    else
    {
        [[[UIAlertView alloc] initWithTitle:@"Wrong Credentials" message:@"TC No or Password is incorrect." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        self.loginButton.enabled = YES;
        self.fullnameTextfield.enabled = YES;
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if ([textField isEqual:self.tcnoTextfield])
    {
        PFQuery *query = [PFUser query];
        [query whereKey:@"username" equalTo:self.tcnoTextfield.text];
        
        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            if (!error)
            {
                if (objects.count > 0)
                {
                    PFQuery *patientQuery = [Patient query];
                    [patientQuery whereKey:@"user" equalTo:[objects firstObject]];
                    
                    [patientQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
                        if (objects.count > 0)
                        {
                            self.fullnameTextfield.text = ((Patient *)[objects firstObject]).fullName;
                            self.fullnameTextfield.enabled = NO;
                        }
                    }];
                }
            }
        }];
    }
}

-(IBAction)signup:(id)sender
{
    NSString *username = [self.tcnoTextfield.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *password = [self.passwordTextfield.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if (username.length < 11)
    {
        [[[UIAlertView alloc] initWithTitle:@"Warning!" message:@"TC No must be exactly 11 characters." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        
        return;
    }
    if (password.length < 2)
    {
        [[[UIAlertView alloc] initWithTitle:@"Warning!" message:@"Password must be at least 2 characters." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        
        return;
    }
    
    self.signupButton.enabled = NO;
    self.user.username = username;
    self.user.password = password;
    
    if([self.user signUp])
    {
        PFQuery *query = [Patient query];
        [query whereKey:@"user" equalTo:[PFUser currentUser]];
        
        NSArray *results = [query findObjects];
        Patient *patient;
        
        if (results.count > 0)
            patient = [results firstObject];
        else
        {
            patient = [Patient object];
            patient.fullName = self.fullnameTextfield.text;
            patient.user = [PFUser currentUser];
            [patient save];
        }
        
        [self performSegueWithIdentifier:@"patientLogin" sender:patient];
    }
    else
    {
        self.signupButton.enabled = YES;
        self.fullnameTextfield.enabled = YES;
        
        [[[UIAlertView alloc] initWithTitle:@"TC No exists!" message:@"Entered TC No exists in our records" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    }
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([textField isEqual:self.tcnoTextfield])
    {
        NSUInteger newLength = textField.text.length + string.length - range.length;
        return newLength>11?NO:YES;
    }
    
    return YES;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    self.loginButton.enabled = YES;
    self.signupButton.enabled = YES;
    self.fullnameTextfield.enabled = YES;
    
    GetAppointmentViewController *vc = segue.destinationViewController;
    vc.patient = sender;
}

@end
